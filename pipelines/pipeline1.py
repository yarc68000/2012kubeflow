# from https://github.com/kubeflow/website/blob/master/content/en/docs/pipelines/sdk/python-function-components.ipynb

import kfp
import kfp.components as comp
import kfp.dsl as dsl

generated="generated/"

def add(a: float, b: float) -> float:
  '''Calculates sum of two arguments'''
  return a + b

add_op = comp.create_component_from_func(
    add, output_component_file=generated + 'add_component.yaml')

@dsl.pipeline(
  name='Addition pipeline',
  description='An example pipeline that performs addition calculations.'
)
def add_pipeline(
  a='1',
  b='7',
):
  # Passes a pipeline parameter and a constant value to the `add_op` factory
  # function.
  first_add_task = add_op(a, 4)
  # Passes an output reference from `first_add_task` and a pipeline parameter
  # to the `add_op` factory function. For operations with a single return
  # value, the output reference can be accessed as `task.output` or
  # `task.outputs['output_name']`.
  second_add_task = add_op(first_add_task.output, b)

kfp.compiler.Compiler().compile(add_pipeline,
  generated + '210101-1.zip')