## thoughts

* in the pipeline gui results are served over minio. Hm. What's this? 
* For lightweight components the components/code is embedded in the zip. I.e. The `add-component.yaml` is embedded in the `pipeline1.py` -> `pipeline.yaml`
* note how for the simple code in `pipeline1.py` the function 
* pipeline.yaml's that refer to gcr images seem to work without any further iam/access mgmt. (clusters made over ai marketplace with all cloud APIs)
* the pipelines components log is nice. you get to see all that happens inside a component. good for debugging. 
* pipeline versioning works nicely. you may upload a new `pipeline.yaml` as a new version on an existing pipeline. 
* pipeline run caches are painful. There doesn't seem to be a way to rerun a component without relying on the cache in the GUI. 
    * You cannot circumvent this with making a new pipeline version, deleting a pipeline, nor by making a new pipeline. The step is still cached. 
* tried to run with default cluster and then a gpu enabled pool. Cluster went into "repair" at first run and then the run executed, but without gpu, `'libcuda.so.1'; dlerror: libcuda.so.1: cannot open shared object file: No such file or directory`


# 210102 kubeflow pipelines gcp easy start

Use https://console.cloud.google.com/marketplace/details/google-cloud-ai-platform/kubeflow-pipelines

Check "Allow access to the following Cloud APIs" and leave rest. Press "create cluster"

When cluster is ready, press "Deploy". 

When deployment is done, go to https://console.cloud.google.com/ai-platform/pipelines/clusters

Click "open pipelines dashboard". You're up and running. 
	 
# 210102 building components gradually

(all presupposing you do this in a reasonable linux/bash environment)

1. Started with lightweight - `pipeline1.py`
1. Tried clients, two types; 
    1. `pipeline1-client.py` - exploring the limited python sdk capabilties, used over GCP
    1. `pipeline1-client.sh` - exploring how to invoke an existing experiment with a new run over REST
1. Did own image, ripping mnist code and making it entirely self-contained (no buckets, only gcr dependency) in `component_mnist`
    1. set `export PROJECT_ID=XXX` in your environment, run `docker_build.sh` and run `pipeline.py`
1. Expanded `component_mnist` in `component_mnist_bucket` with writing a log result and model in a bucket
1. Added a `component_mnist_bucket_gpu` version
    1. Follow https://www.kubeflow.org/docs/gke/pipelines/enable-gpu-and-tpu/, https://www.kubeflow.org/docs/gke/customizing-gke/#common-customizations
    1. Dockerfile built on gpu enabled image
    1. `pipeline.py` has a gpu code part that translates to nvidia/gpu pod spec. 
    1. Added a GPU node pool
    1. Added NVIDIA drivers 
    1. (Removed the autoscaling)
1. Added a `component_mnist_bucket_metrics`. The metrics are show in the experiment overview of the pipeline ui. The trick here is twofold:
    1. You need to write a metrics json file to a local path in the container code (`mnist.py`)
    1. You need to have an output parameter in the `dsl.ContainerOp()` function which is named precisely `mlpipeline-metrics` referring to the local path. 
    


## 210104 GPU run (step 5)

```
WARNING:tensorflow:From /usr/local/lib/python3.6/dist-packages/tensorflow_core/python/ops/resource_variable_ops.py:1630: calling BaseResourceVariable.__init__ (from tensorflow.python.ops.resource_variable_ops) with constraint is deprecated and will be removed in a future version.
Instructions for updating:
If using Keras pass *_constraint arguments to layers.
Model: "sequential"
_________________________________________________________________
Layer (type)                 Output Shape              Param #   
=================================================================
flatten (Flatten)            (None, 784)               0         
_________________________________________________________________
dense (Dense)                (None, 512)               401920    
_________________________________________________________________
dropout (Dropout)            (None, 512)               0         
_________________________________________________________________
dense_1 (Dense)              (None, 10)                5130      
=================================================================
Total params: 407,050
Trainable params: 407,050
Non-trainable params: 0
_________________________________________________________________
None
Downloading data from 
https://storage.googleapis.com/tensorflow/tf-keras-datasets/mnist.npz

    8192/11490434 [..............................] - ETA: 0s
 4202496/11490434 [=========>....................] - ETA: 0s
11493376/11490434 [==============================] - 0s 0us/step
2021-01-04 20:43:39.127683: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcuda.so.1
2021-01-04 20:43:39.149402: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.150279: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1639] Found device 0 with properties: 
name: Tesla K80 major: 3 minor: 7 memoryClockRate(GHz): 0.8235
pciBusID: 0000:00:04.0
2021-01-04 20:43:39.150804: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudart.so.10.0
2021-01-04 20:43:39.152558: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcublas.so.10.0
2021-01-04 20:43:39.153938: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcufft.so.10.0
2021-01-04 20:43:39.154477: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcurand.so.10.0
2021-01-04 20:43:39.156466: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusolver.so.10.0
2021-01-04 20:43:39.158063: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusparse.so.10.0
2021-01-04 20:43:39.163389: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudnn.so.7
2021-01-04 20:43:39.163550: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.164502: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.165301: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1767] Adding visible gpu devices: 0
2021-01-04 20:43:39.165953: I tensorflow/core/platform/cpu_feature_guard.cc:142] Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA
2021-01-04 20:43:39.173217: I tensorflow/core/platform/profile_utils/cpu_utils.cc:94] CPU Frequency: 2300000000 Hz
2021-01-04 20:43:39.173725: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x5287bc0 initialized for platform Host (this does not guarantee that XLA will be used). Devices:
2021-01-04 20:43:39.173759: I tensorflow/compiler/xla/service/service.cc:176]   StreamExecutor device (0): Host, Default Version
2021-01-04 20:43:39.234130: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.235085: I tensorflow/compiler/xla/service/service.cc:168] XLA service 0x59b5970 initialized for platform CUDA (this does not guarantee that XLA will be used). Devices:
2021-01-04 20:43:39.235122: I tensorflow/compiler/xla/service/service.cc:176]   StreamExecutor device (0): Tesla K80, Compute Capability 3.7
2021-01-04 20:43:39.235374: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.236224: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1639] Found device 0 with properties: 
name: Tesla K80 major: 3 minor: 7 memoryClockRate(GHz): 0.8235
pciBusID: 0000:00:04.0
2021-01-04 20:43:39.236288: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudart.so.10.0
2021-01-04 20:43:39.236322: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcublas.so.10.0
2021-01-04 20:43:39.236349: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcufft.so.10.0
2021-01-04 20:43:39.236398: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcurand.so.10.0
2021-01-04 20:43:39.236425: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusolver.so.10.0
2021-01-04 20:43:39.236449: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcusparse.so.10.0
2021-01-04 20:43:39.236478: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudnn.so.7
2021-01-04 20:43:39.236589: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.237457: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.238185: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1767] Adding visible gpu devices: 0
2021-01-04 20:43:39.238235: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcudart.so.10.0
2021-01-04 20:43:39.239591: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1180] Device interconnect StreamExecutor with strength 1 edge matrix:
2021-01-04 20:43:39.239624: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1186]      0 
2021-01-04 20:43:39.239639: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1199] 0:   N 
2021-01-04 20:43:39.239866: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.240775: I tensorflow/stream_executor/cuda/cuda_gpu_executor.cc:983] successful NUMA node read from SysFS had negative value (-1), but there must be at least one NUMA node, so returning NUMA node zero
2021-01-04 20:43:39.241585: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1325] Created TensorFlow device (/job:localhost/replica:0/task:0/device:GPU:0 with 10805 MB memory) -> physical GPU (device: 0, name: Tesla K80, pci bus id: 0000:00:04.0, compute capability: 3.7)
2021-01-04 20:43:40.118982: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcublas.so.10.0
2021-01-04 20:43:40.302601: I tensorflow/core/profiler/lib/profiler_session.cc:205] Profiler session started.
2021-01-04 20:43:40.304006: I tensorflow/stream_executor/platform/default/dso_loader.cc:44] Successfully opened dynamic library libcupti.so.10.0
Train on 60000 samples, validate on 10000 samples
Epoch 1/5
2021-01-04 20:43:40.439858: I tensorflow/core/platform/default/device_tracer.cc:588] Collecting 63 kernel records, 6 memcpy records.
WARNING:tensorflow:Method (on_train_batch_end) is slow compared to the batch update (3.038502). Check your callbacks.
WARNING:tensorflow:Method (on_train_batch_end) is slow compared to the batch update (0.132295). Check your callbacks.

   32/60000 [..............................] - ETA: 12:01 - loss: 2.4133 - acc: 0.0625
 <..>
>60000/60000 [==============================] - 7s 124us/sample - loss: 0.0444 - acc: 0.9855 - val_loss: 0.0655 - val_acc: 0.9805
copying mnist_model.h5 to gs://mfo201229/mnist_model.h5
```

