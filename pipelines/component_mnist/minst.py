# cf. https://github.com/kubeflow/pipelines/blob/master/samples/contrib/mnist/02_Local_Development_with_Docker_Image_Components.ipynb

# import argparse
from datetime import datetime
import tensorflow as tf


bucket="/tmp"
model_file="staticmodel.h5"

model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(512, activation=tf.nn.relu),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10, activation=tf.nn.softmax)
])

model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

print(model.summary())

mnist = tf.keras.datasets.mnist
(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

callbacks = [
  tf.keras.callbacks.TensorBoard(log_dir=bucket + '/logs/' + datetime.now().date().__str__()),
  # Interrupt training if val_loss stops improving for over 2 epochs
  tf.keras.callbacks.EarlyStopping(patience=2, monitor='val_loss'),
]

model.fit(x_train, y_train, batch_size=32, epochs=5, callbacks=callbacks,
          validation_data=(x_test, y_test))

gcs_path = bucket + "/" + model_file

model.save(gcs_path)

# from tensorflow import gfile


# if gfile.Exists(gcs_path):
#     gfile.Remove(gcs_path)

# gfile.Copy(model_file, gcs_path)
with open('/tmp/output.txt', 'w') as f:
  f.write(gcs_path)