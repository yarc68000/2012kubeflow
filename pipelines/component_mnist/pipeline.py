# import kfp.gcp as gcp
import kfp
import kfp.dsl as dsl
import os

PROJECT_ID=os.environ['PROJECT_ID']


def mnist_train_op():
    return dsl.ContainerOp(
      name="mnist_training_container",
      image='gcr.io/' + PROJECT_ID + '/kfp_component_mnist:0.1',
      command=['python', '/app/minst.py'],
    )

# Define the pipeline
@dsl.pipeline(
   name='Mnist pipeline',
   description='A toy pipeline that performs mnist model training.'
)
def mnist_container_pipeline():
    mnist_train_op() # .apply(gcp.use_gcp_secret('user-gcp-sa'))

kfp.compiler.Compiler().compile(mnist_container_pipeline,
  "../generated/210102mnist.zip")