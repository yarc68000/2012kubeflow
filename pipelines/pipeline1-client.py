# sdk doc: https://kubeflow-pipelines.readthedocs.io/en/stable/source/kfp.client.html

HOSTURL="XXX" # eg. 0023A33a-dfreeb7cs4b-dot-europe-west1.pipelines.googleusercontent.com, taken from from https://console.cloud.google.com/ai-platform/pipelines/clusters

import kfp
client = kfp.Client(host=HOSTURL)

# print(client.list_pipelines())

print(client.run_pipeline(experiment_id="210101exp1", job_name="210101run2"))