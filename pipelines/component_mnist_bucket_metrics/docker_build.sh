#!/bin/bash

# you need to set export PROJECT_ID outside this script
export IMAGE_NAME=kfp_component_mnist
TAG=0.5
GCR_IMAGE=gcr.io/$PROJECT_ID/$IMAGE_NAME:$TAG
echo $building $GCR_IMAGE
docker build -t $IMAGE_NAME .
docker tag $IMAGE_NAME $GCR_IMAGE
docker push $GCR_IMAGE
# docker image rm \${IMAGE_NAME}
# docker image rm \${GCR_IMAGE}