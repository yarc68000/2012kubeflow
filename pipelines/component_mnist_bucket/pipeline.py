# import kfp.gcp as gcp
import kfp
import kfp.dsl as dsl
import os

PROJECT_ID=os.environ['PROJECT_ID']


GCS_BUCKET='gs://mfo201229'

def mnist_train_op(model_file, bucket):
    return dsl.ContainerOp(
      name="mnist_training_container",
      image='gcr.io/' + PROJECT_ID + '/kfp_component_mnist:0.2',
      command=['python', '/app/minst.py'],
      file_outputs={'outputs': '/tmp/output.txt'},
      arguments=['--bucket', bucket, '--model_file', model_file]
    )

# Define the pipeline
@dsl.pipeline(
   name='Mnist pipeline',
   description='A toy pipeline that performs mnist model training.'
)
def mnist_container_pipeline(
        model_file: str = 'mnist_model.h5',
        bucket: str = GCS_BUCKET):
    mnist_train_op(model_file=model_file, bucket=bucket) # .apply(gcp.use_gcp_secret('user-gcp-sa'))

kfp.compiler.Compiler().compile(mnist_container_pipeline,
  "../generated/210103mnist_bucket.zip")