#!/bin/bash

# doc: https://www.kubeflow.org/docs/pipelines/tutorials/api-pipelines/

# you need to have a port forward going;
# $ export SVC_PORT=$(kubectl -n default get svc/ml-pipeline -o json | jq ".spec.ports[0].port")
# $ kubectl port-forward -n default svc/ml-pipeline ${SVC_PORT}:8888
# Forwarding from 127.0.0.1:8888 -> 8888
# Forwarding from [::1]:8888 -> 8888
# Handling connection for 8888

export PIPELINE_ID=xxx # picked out from the ui
export SVC=localhost:8888

# list pipelines

curl ${SVC}/apis/v1beta1/pipelines/${PIPELINE_ID} | jq

# run a pipeline

RUN_ID=$((
curl -H "Content-Type: application/json" -X POST ${SVC}/apis/v1beta1/runs \
-d @- << EOF
{
   "name":"${PIPELINE_NAME}_run",
   "pipeline_spec":{
      "pipeline_id":"${PIPELINE_ID}"
   }
}
EOF
) | jq -r .run.id)