#!/bin/bash
# https://www.kubeflow.org/docs/gke/customizing-gke/#common-customizations

export GPU_POOL_NAME=mfo210103gpu
export KF_NAME=cluster-1

gcloud container node-pools create ${GPU_POOL_NAME} \
--accelerator type=nvidia-tesla-k80,count=1 \
--zone us-central1-a --cluster ${KF_NAME} \
--scopes "https://www.googleapis.com/auth/cloud-platform" \
--num-nodes=1 --machine-type=n1-standard-4 # --min-nodes=0 --max-nodes=5 --enable-autoscaling

kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/container-engine-accelerators/master/nvidia-driver-installer/cos/daemonset-preloaded.yaml
